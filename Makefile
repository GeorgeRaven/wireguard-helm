CHART_DIR_PATH="charts/wireguard"
CHART_NAME="wireguard"
CHART_NAMESPACE="wireguard"
FORWARD_PORT="51820"
PRIVATE_REGISTRY="registry.gitlab.com"

.PHONY: all
all: lint minikube install

.PHONY: lint
lint: deps
	helm lint ${CHART_DIR_PATH}/.

.PHONY: deps
deps:
	helm dependency update ${CHART_DIR_PATH}/.

.PHONY: minikube
minikube:
	minikube delete
	minikube start
	# minikube addons enable ingress

.PHONY: ingress
ingress:
	minikube addons enable ingress

# .PHONY: login
# login: login.lock
#
# login.lock:
# 	podman login ${PRIVATE_REGISTRY}
# 	kubectl create -n ${CHART_NAMESPACE} secret generic regcred --from-file=.dockerconfigjson=${HOME}/.docker/config.json --type=kubernetes.io/dockerconfigjson --dry-run=client -o yaml > login.creds
# 	podman logout {PRIVATE_REGISTRY}
# 	touch login.lock

.PHONY: test
test: lint minikube install

.PHONY: template
template: templates.yaml

templates.yaml:
	helm template --set namespace.name=${CHART_NAMESPACE} --set namespace.create=true ${CHART_DIR_PATH}/. > templates.yaml

.PHONY: install
install: # login.lock
	kubectl create namespace ${CHART_NAMESPACE}
	# kubectl apply -f login.creds
	# kubectl get -n ${CHART_NAMESPACE} secret regcred --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode
	helm install --namespace ${CHART_NAMESPACE} ${CHART_NAME} ${CHART_DIR_PATH}/.

.PHONY: upgrade
upgrade:
	helm upgrade --namespace ${CHART_NAMESPACE} ${CHART_NAME} ${CHART_DIR_PATH}/.


.PHONY: uninstall
uninstall:
	helm uninstall --namespace ${CHART_NAMESPACE} ${CHART_NAME}
	kubectl delete namespace ${CHART_NAMESPACE}

.PHONY: clean
clean:
	rm -f login.lock login.creds templates.yaml
	minikube delete
	# podman logout {PRIVATE_REGISTRY}

.PHONY: stuck
stuck:
	kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n ${CHART_NAMESPACE}
